#!/usr/bin/env python
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Copyright 2020 Red Hat, Inc.

from __future__ import print_function
import argparse
import errno
import fcntl
import getpass
import logging
import os
import select
import pyudev
import shutil
import signal
import struct
import stat
import subprocess
import threading
import time

# The root path our directories will be created within
# We're using /home/kahlschlag because tmpfs may run out of space really
# quickly, we definitely don't want / to fill up with our log files and on
# the machine(s) we need to test this /home is on a separate partition.
ROOT_PATH = '/home/kahlschlag'
# base directory is timestamped
BASE_DIRECTORY = None

logger = None


def relpath(path):
    return path.lstrip('/')


# GitPython is only on EPEL
class Git(object):
    def __init__(self, path):
        self.path = path

    def init(self):
        self._run(['git', 'init', '--quiet'])
        self._run(['git', 'config', '--local',
                   'user.name', 'kahlschlag'])
        self._run(['git', 'config', '--local',
                   'user.email', 'noreply@email.none'])

    def add_all(self):
        self._run(['git', 'add', '--all', '.'])

    def commit(self, msg):
        self._run(['git', 'commit',
                   '--quiet', '-a',
                   '-m', msg])

    def checkout(self):
        self._run(['git', 'checkout', '-f'])

    def _run(self, args):
        p = subprocess.Popen(args, cwd=self.path,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        stdout = p.stdout.read()
        if stdout:
            logger.info(stdout)
        stderr = p.stderr.read()
        if stderr:
            logger.warning(stderr)


class MonitoredObject(object):
    '''
    Parent class for all objects we care about in the logger.
    '''
    def __init__(self, basedir=None):
        # We have to set this at runtime because we change BASE_DIRECTORY
        # from None to the actual base directory in main()
        self.basedir = basedir or BASE_DIRECTORY
        self.dead = False

    def make_dest(self, *paths):
        '''
        Return the destination path inside the base directory.
        '''
        path = os.path.join(*paths)
        dest = os.path.join(self.basedir, relpath(path))
        dirname = os.path.dirname(dest)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        return dest

    def start(self):
        pass

    def stop(self):
        pass

    def update(self):
        '''
        This method is called on each polling iteration. Copy the files,
        update the files, etc. during update so the current state can be
        committed..
        '''
        pass

    def remove(self):
        '''
        This method should remove the destination file and/or directory
        where appropriate.
        '''
        pass


class EventRecorder(MonitoredObject, threading.Thread):
    '''
    Records the events from a device node into a yaml file, roughly
    resembling libinput-record's output.

    We want the devices to be recorded in realtime (not just every interval
    seconds) so this runs in a separate thread. Thus, the update() method
    does nothing - we only do something on errors or on stop().
    '''
    def __init__(self, node, basedir=None):
        threading.Thread.__init__(self)
        MonitoredObject.__init__(self, basedir)
        self.event_node = node
        self.dest = self.make_dest(node)
        self.outfile = open(self.dest, 'w')
        self._write_header(self.outfile)
        # We use a pipe instead of Event to avoid the busy loop. we can't
        # poll() on a threading.Event and most of our event recordings will
        # be mute anyway.
        self.wakeup_pipe = os.pipe()
        self.daemon = True

    def _write_header(self, fd):
        fd.write('device: {}\n'.format(self.event_node))
        fd.write('timestamp: {}\n'.format(Timestamps.timestamp()))
        fd.write('events:\n')
        fd.flush()

    def start(self):
        # name collision here, so we need to invoke this specifically
        threading.Thread.start(self)

    def run(self):
        fd = open(self.event_node, 'rb')
        fl = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

        wakeupfd = self.wakeup_pipe[0]

        poll = select.epoll()
        poll.register(fd, eventmask=select.EPOLLIN)
        poll.register(wakeupfd, eventmask=select.EPOLLIN)

        # struct input_event {
        #       struct timeval time; (2 longs)
        #       __u16 type;
        #       __u16 code;
        #       __s32 value;
        # }
        eventstruct = struct.Struct('LLHHi')
        while True:
            fds = poll.poll()
            if wakeupfd in [x[0] for x in fds]:
                break

            # not sure this can happen, but if we get past poll but have no
            # events, something odd is happening so we might as well quit.
            if not fds:
                break

            try:
                bs = fd.read()
                offset = 0
                while offset < len(bs):
                    event = eventstruct.unpack_from(bs, offset)
                    self.write_event(event)
                    offset += eventstruct.size
            except IOError as e:
                import errno
                if e.errno == errno.ENODEV:
                    logger.debug('Device {} disappeared'.format(self.event_node))
                    self.outfile.write('# ENODEV\n')
                    break
                if e.errno != errno.EAGAIN:
                    raise e
        self.outfile.write('# recording finished')
        self.dead = True

        try:
            os.close(self.wakeup_pipe[0])
            os.close(self.wakeup_pipe[1])
        except OSError:
            pass

        # if the device is already unplugged, close() causes an exception.
        # Handle that here explicitly, because we don't want this to happen
        # during __del__.
        try:
            fd.close()
        except IOError:
            pass

    def write_event(self, event):
        # obfuscate event code for KEY_1 .. KEY_ZENKAKUHANKAKU
        if event[2] == 0x01 and 2 <= event[3] < 85:
            event = list(event)
            event[3] = 30  # KEY_A
        # and event value for MSC_SCAN
        elif event[2] == 0x04 and event[3] == 0x04:
            event = list(event)  # can't change a tuple
            event[4] = 30  # KEY_A

        self.outfile.write('  - event: [{}]\n'.format(', '.join([str(x) for x in event])))
        self.outfile.flush()

    def stop(self):
        if self.dead:
            return

        self.dead = True
        try:
            boinker = os.fdopen(self.wakeup_pipe[1], 'w')
            boinker.write('boink\n')
            boinker.flush()
            boinker.close()
        except OSError:
            pass
        self.join(2)

    def __str__(self):
        return 'evemu {}'.format(self.event_node)

    def __del__(self):
        self.stop()


class Timestamps(MonitoredObject):
    '''
    Single YAML file that logs a timestamp on each call to update(). The
    purpose of this class is to have at least one change on every iteration,
    indicating that this logger is still running as expected.

    .. attribute:: current_timestamp

        The most recently logged timestamp. Useful to e.g. keep the git
        commit message in sync with the timestamps file.
    '''
    def __init__(self, start_time, basedir=None):
        super(Timestamps, self).__init__(basedir)
        self.current_timestamp = start_time
        self.dest = self.make_dest('timestamps')
        self.fd = open(self.dest, 'w')
        self.fd.write('timestamps:\n')
        self._write_timestamp(start_time, tstype='start')

    def _write_timestamp(self, ts, tstype=None):
        self.fd.write('  - time: {}\n'.format(ts))
        if tstype is not None:
            self.fd.write('    type: {}\n'.format(tstype))
        self.fd.flush()

    def update(self):
        self.current_timestamp = Timestamps.timestamp()
        self._write_timestamp(self.current_timestamp)

    def stop(self):
        self.current_timestamp = Timestamps.timestamp()
        self._write_timestamp(self.current_timestamp, tstype='shutdown')

    @classmethod
    def timestamp(cls):
        '''This method just exists so we always have int timestamps'''
        return int(time.time())

    def __str__(self):
        return 'Timestamp {}'.format(self.current_timestamp)


class MonitoredFile(MonitoredObject):
    '''Copies the given file into our basedir with the same path otherwise.
       Whenever update() is called, the source file is re-copied.

       .. attribute:: dead

           Always False, if the file disappeared it will be monitored again
           if/when it comes comes back.
    '''
    def __init__(self, path, basedir=None):
        super(MonitoredFile, self).__init__(basedir)
        self.source = path
        self.dest = self.make_dest(path)
        self.update()
        # If the file never existed, create it so we know we did monitor it,
        # it just never showed up. This way we at least record that we
        # should've done something with it. This of course relies on a git
        # commit being run before remove() is called, which does remove it
        # again.
        if not os.path.exists(self.source):
            with open(self.dest, 'w') as fd:
                fd.write('file does not exist\n')

    def update(self):
        if not os.path.exists(self.source):
            self.remove()
        else:
            shutil.copy2(self.source, self.dest)

    def remove(self):
        try:
            os.unlink(self.dest)
        except OSError:
            pass

    def __str__(self):
        return self.source


class MonitoredDirectory(MonitoredObject):
    '''Recursively copies the directory into our basedir with the same
       directory structure otherwise.

       .. attribute:: dead

           The source directory has disappeared, calling update() or
           remove() does nothing.
    '''
    def __init__(self, path, basedir=BASE_DIRECTORY):
        super(MonitoredDirectory, self).__init__(basedir)
        self.source = path
        self.dest = self.make_dest(path)
        self.update()

    def update(self):
        if self.dead:
            return
        elif not os.path.exists(self.source):
            self.remove()
            self.dead = True
            return

        def ignore(directory, files):
            '''returns the list of files within directory that should be
               ignored during shutil.copytree'''

            # zero-sized files (but not directories) cause copytree errors,
            # ignore those
            def zerosize(filename):
                st = os.stat(os.path.join(directory, filename))

                # If we can't read the file, ignore it
                if not stat.S_IMODE(st.st_mode) & stat.S_IREAD:
                    return True

                if stat.S_ISDIR(st.st_mode):
                    return False

                return st.st_size == 0

            # autosuspend_delay_ms gives us IO errors when copying
            return [f for f in files if zerosize(f) or f == 'autosuspend_delay_ms']

        try:
            shutil.copytree(self.source, self.dest, symlinks=True, ignore=ignore)
        except OSError as e:
            # race condition: on device removal the syspath may disappear
            # during copytree. Let's ignore that
            if e.errno == errno.ENOENT:
                pass

    def remove(self):
        if self.dead:
            return

        try:
            shutil.rmtree(self.dest)
        except OSError as e:
            # race condition: on device removal the syspath may disappear
            # during copytree. Let's ignore that
            if e.errno == errno.ENOENT:
                pass

    def __str__(self):
        return self.source


class MonitoredCommand(MonitoredObject):
    _counter = 0
    '''Runs a command, waits for it to exit and dumps output and return
       value of the command into <commandname>.stdout, etc. Running update()
       will re-run the same command and overwrite those files with the most
       recent output.

       This should only be used for commands that finish immediately. Use
       MonitoredTail for commands that keep running.
    '''
    def __init__(self, cmd, basedir=BASE_DIRECTORY):
        super(MonitoredCommand, self).__init__(basedir)
        self.cmd = cmd
        self.name = os.path.basename(cmd[0])
        # The command may have to be run multiple times with different
        # arguments, hence we end up with:
        #   0-command/command.invocation  - carries the command passed in
        #   0-command/command.stdout
        #   0-command/command.stderr
        # etc.
        self.dest = self.make_dest('{}-{}'.format(MonitoredCommand._counter, self.name), self.name)
        with open('{}.invocation'.format(self.dest), 'w') as fd:
            fd.write(' '.join(cmd))
            fd.write('\n')
        self.update()
        MonitoredCommand._counter += 1

    def update(self):
        p = subprocess.Popen(self.cmd, cwd='/',
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        with open('{}.exitcode'.format(self.dest), 'w') as fd:
            fd.write('{}'.format(p.returncode))
        with open('{}.stdout'.format(self.dest), 'w') as fd:
            stdout = p.stdout.read()
            fd.write(stdout)
        with open('{}.stderr'.format(self.dest), 'w') as fd:
            stderr = p.stderr.read()
            fd.write(stderr)

    def remove(self):
        for suffix in ['exitcode', 'stdout', 'stderr']:
            os.unlink('{}.{}'.format(self.dest, suffix))

    def __str__(self):
        return ' '.join(self.cmd)


class MonitoredTail(MonitoredCommand):
    '''Runs a command in the background, setting it up so that its stdout
       and stderr go to into <commandname>.stdout, etc. Running update()
       will restart the command if it has exited and overwrite the previous
       values.
    '''
    def __init__(self, cmd, basedir=BASE_DIRECTORY):
        self.process = None
        super(MonitoredTail, self).__init__(cmd, basedir)
        self.start()

    def start(self):
        self.stdout = open('{}.stdout'.format(self.dest), 'w')
        self.stderr = open('{}.stderr'.format(self.dest), 'w')
        self.process = subprocess.Popen(['stdbuf', '-o0', '-e0'] + self.cmd,
                                        cwd='/',
                                        stdout=self.stdout,
                                        stderr=self.stderr)

    def update(self):
        if self.process is None:  # super constructor calls update() before start()
            return

        if self.process.poll() is not None:
            logger.debug('Tailed command {} has terminated ({}).  Restarting'.format(self, self.process.returncode))
            self.stdout.close()
            self.stderr.close()
            self.start()

    def remove(self):
        pass

    def stop(self):
        self.process.terminate()
        logger.debug('Terminated {}'.format(self))


def make_base_directory(root_path, start_time, debug=False):
    if debug:
        timestamp = 'debug'
    else:
        timestamp = time.strftime('%Y-%m-%d-%H:%M:%S', time.gmtime(start_time))
    dirname = os.path.join(root_path, 'kahlschlag-{}'.format(timestamp))
    if debug:
        if os.path.exists(dirname):
            shutil.rmtree(dirname)
        logger.warning('Debugging file tree in use!')
        logger.warning('Removing previous tree: {}'.format(dirname))
    os.makedirs(dirname)

    return dirname


def init_logging(debug):
    level = logging.DEBUG if debug else logging.INFO
    logger = logging.getLogger('logcollect')
    logger.setLevel(level)
    fmt = logging.Formatter('%(levelname)-8s| %(message)s')
    ch = logging.StreamHandler()
    ch.setLevel(level)
    ch.setFormatter(fmt)
    logger.addHandler(ch)
    return logger


def handle_udev_event(action, device):
    logger.debug('udev event {} for {}'.format(action, device.sys_path))
    if action != 'add':
        return []

    objects = []
    if 'event' in device.sys_path:
        evemu = EventRecorder(device.device_node)
        evemu.start()
        objects.append(evemu)

        if '/sys/devices/virtual/input/' in device.sys_path:
            # uinput devices need special treatment
            parent = device
            while parent.parent is not None:
                parent = parent.parent

                logger.debug('Setting up directory monitor for virtual device {}'.format(device.sys_path))
                d = MonitoredDirectory(device.sys_path)
                objects.append(d)

        else:
            # a normal device, we go up to the highest parent that is a usb
            # device and monitor that. This is usually the hub so we're
            # doubly-monitoring all devices but meh.
            parent = device
            while parent and parent.device_type != 'usb_device':
                parent = parent.parent

            if parent is not None:
                logger.debug('Setting up directory monitor for USB device {}'.format(parent.sys_path))
                d = MonitoredDirectory(parent.sys_path)
                objects.append(d)

    return objects


def mainloop(git, tsfile, interval, stop_after):
    paths = [
        '/proc/bus/input/devices',
        '/var/log/Xorg.0.log',
    ]
    paths += ['/var/log/Xorg.{}.log'.format(i) for i in range(10)]

    # /sys/bus/{usb|hid} just symlinks to /sys/devices, so we just copy that
    # tree with symlinks and worry about the actual device filtering through
    # udev.
    directories = [
        '/sys/bus/usb',
        '/sys/bus/hid',
    ]

    commands = [
        ['/usr/bin/lsusb', '-vv'],
        ['/usr/bin/xinput', 'list', '--long'],
    ]

    # FIXME: better to read this directly
    tails = [
        ['/usr/bin/dmesg', '--follow', '--ctime'],
    ]
    # We could figure out which devices are connected, or we could just
    # record the first 20 devices and hope the interesting one is within
    # that set.
    # device ids 0-5 are reserved, reserved, VCP, VCK and the two xtest
    # devices, we don't have to care about those.
    tails += [['/usr/bin/xinput', 'watch-props', str(i)] for i in range(6, 25)]

    stop_time = tsfile.current_timestamp + 60 * stop_after if stop_after else None
    if stop_time:
        logger.debug('Will be stopping at timestamp {} in {}min'.format(stop_time, stop_after))

    # slight hack: tsfile needs to be the first element in the list so the
    # timestamp we re-use from is updated first before we do anything else.
    objects = [tsfile]
    for path in paths:
        logger.debug('Setting up file monitor for {}'.format(path))
        objects.append(MonitoredFile(path))

    for path in directories:
        logger.debug('Setting up directory monitor for {}'.format(path))
        objects.append(MonitoredDirectory(path))

    for command in commands:
        logger.debug('Setting up command logger for {}'.format(' '.join(command)))
        objects.append(MonitoredCommand(command))

    for tail in tails:
        logger.debug('Setting up tail for {}'.format(' '.join(tail)))
        objects.append(MonitoredTail(tail))

    # we are interested in the USB devices that provide our HID devices. we
    # go to the highest parent that is a USB device (i.e. the hub) and add
    # that as a directory. Since all input devices will be subdirs of that
    # we'll end up with multiple monitors for the same directory (once
    # initiated by the HID system here, then by the input system below,
    # etc)
    # FIXME: should be handled by handle_udev_event
    udev = pyudev.Context()
    for device in udev.list_devices(subsystem='hid'):
        parent = device
        while parent and parent.device_type != 'usb_device':
            parent = parent.parent

        if parent is not None:
            logger.debug('Setting up directory monitor for USB device {}'.format(parent.sys_path))
            d = MonitoredDirectory(parent.sys_path)
            objects.append(d)

    # and we're interested in any actual event node, because uinput doesn't
    # show in the hid subsystem.
    #
    for device in udev.list_devices(subsystem='input'):
        if device.device_node is not None and device.device_node.startswith('/dev/input/event'):
            o = handle_udev_event('add', device)
            if o:
                objects += o

    # All data is stored in git for every iteration. git is much better at
    # tracking changes that our script could ever be.
    git.init()
    git.add_all()
    git.commit('Initial directory tree')
    for o in objects:
        o.remove()

    monitor = pyudev.Monitor.from_netlink(udev)
    monitor.filter_by('input')
    monitor.start()
    udevpoll = select.epoll()
    udevpoll.register(monitor.fileno(), eventmask=select.EPOLLIN)

    def sighandler(sig, frame):
        # SIGTERM, SIGINT, it's all the same to us, we want to quit.
        raise KeyboardInterrupt()

    signal.signal(signal.SIGTERM, sighandler)

    try:
        while True:
            # By default we snapshot every 30 seconds, but where we
            # have a udev event we take a snapshot at that time - adding
            # devices is significant...
            rc = udevpoll.poll(timeout=interval)

            # The pyudev version in RHEL7 is terrible. We can only receive a
            # single device per loop and it'll block when there is no data
            # so we have to be really sure there is something for it to
            # read.
            while rc:
                action, device = monitor.receive_device()
                if device:
                    handle_udev_event(action, device)
                # timeout of 1 here because we need all udev events from
                # this batch but then immediately update all other things
                # that we monitor
                rc = udevpoll.poll(timeout=1)

            # Now update all monitored objects
            # Note: The Updating Timestamp message is always behind by one
            for o in objects:
                logger.debug('Updating {}'.format(o))
                o.update()
            git.add_all()

            if stop_time and tsfile.current_timestamp > stop_time:
                logger.debug('Terminating at expiry')
                git.commit('Last iteration at {} before stop time'.format(tsfile.current_timestamp))
                break
            else:
                git.commit('Iteration at {}'.format(tsfile.current_timestamp))

            # At the end of each iteration we remove all tracked files.
            # objects disappearing will thus be recorded in the next git
            # commit. And where removal takes a while, it gives us a
            # headstart for the next iteration's copy so our timestamps are
            # closer to the recorded timestamp.
            for o in list(objects):
                o.remove()
                # Dead objects are those where the monitored item
                # has disappeared
                if o.dead:
                    o.stop()
                    objects.remove(o)

    except KeyboardInterrupt:
        logger.debug('Quitting on user request')
        # We need to update all objects, by default they all had remove()
        # called on them.
        for o in objects:
            logger.debug('Final update for {}'.format(o))
            o.update()
        git.add_all()
        git.commit('Final commit after user quit')
    except Exception as e:
        for o in objects:
            o.stop()
        raise e

    for o in objects:
        o.stop()


def remove_old_logs(basedir, keep):
    '''
    Search basedir for our old log directories and remove all of them bar
    the number specified in keep.
    '''
    if keep < 0:
        return

    if not os.path.exists(basedir):
        return

    entries = [os.path.join(basedir, e) for e in os.listdir(basedir)]

    def is_kahlschlag_directory(path):
        return os.path.basename(path).startswith('kahlschlag-') and os.path.isdir(path)

    ksdirs = sorted([e for e in entries if is_kahlschlag_directory(e)])
    ksdirs.reverse()
    try:
        to_remove = ksdirs[keep:]
    except IndexError:
        return

    for d in to_remove:
        # some sanity checking so we don't remove a directory that's not ours
        assert d.startswith(str(basedir))
        assert os.path.isdir(os.path.join(d, '.git'))
        assert os.path.exists(os.path.join(d, 'timestamps'))
        logger.info('Removing directory {}'.format(d))
        shutil.rmtree(d)


def find_xorg_display():
    display = os.environ.get('DISPLAY')
    if display is None:
        for f in os.listdir('/tmp/.X11-unix/'):
            if f.startswith('X'):
                display = ':{}'.format(int(f[1:]))
                break
        else:
            display = ':0'
        os.environ['DISPLAY'] = display

    username = getpass.getuser()
    logger.info('Using $DISPLAY={}, run xhost si:localuser:{} to allow access'.format(display, username))


def log_host_info(basedir):
    d = os.path.join(basedir, 'host')
    os.makedirs(d)

    hostfiles = [
        '/etc/hostname',
        '/proc/uptime',
        '/proc/sys/kernel/random/boot_id',
        '/etc/machine-id',
    ]

    for f in hostfiles:
        dest = os.path.join(d, os.path.basename(f))
        try:
            shutil.copy(f, dest)
        except IOError:
            pass


def main():
    global BASE_DIRECTORY
    global logger

    description = '''
        Kahlschlag is a system logger designed to monitor input devices.
        It creates a new directory with a timestamp in the --root-path and
        logs all input devices (including sysfs data) to that directory.
        The data itself is tracked with git and can be analyzed for changes
        with normal git tools.

        Kahlschlag directly monitors input, sysfs data is polled every
        --interval seconds.

        To avoid running out of storage space, Kahlschlag will remove old
        log data on startup (see --keep) and terminate automatically after a
        set time. Run it as a systemd service to have it automatically
        restart.
    '''
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--debug', action='store_true', default=False,
                        help='Enable debugging behavior. This stores the data in a fixed-name directory rather than timestamping those to ease debugging. Also enabled --verbose.')
    parser.add_argument('--verbose', action='store_true', default=False,
                        help='Enable verbose logging')
    parser.add_argument('--root-path', type=str, default=ROOT_PATH,
                        help='The directory to create timestamped log directories in')
    parser.add_argument('--keep', type=int, default=10,
                        help='Remove all but the latest N log directories within the root path (default: 10)')
    parser.add_argument('--interval', type=int, default=30,
                        help='Interval in seconds between polling all data (default: 30)')
    parser.add_argument('--stop-after', type=int, default=(5 * 60),
                        help='Automatically exit after N minutes. Set to 0 to never terminate. Default: 300')
    ns = parser.parse_args()

    logger = init_logging(ns.debug or ns.verbose)

    if ns.keep is not None:
        remove_old_logs(ns.root_path, ns.keep)

    start_time = Timestamps.timestamp()
    logger.debug('Start time: {}'.format(start_time))
    BASE_DIRECTORY = make_base_directory(ns.root_path, start_time, debug=ns.debug)
    logger.info('Logging to {}'.format(BASE_DIRECTORY))

    log_host_info(BASE_DIRECTORY)

    timestamps = Timestamps(start_time)

    find_xorg_display()

    git = Git(BASE_DIRECTORY)
    mainloop(git, timestamps, ns.interval, ns.stop_after)

    git.add_all()
    git.commit('Terminating at {}'.format(timestamps.current_timestamp))


if __name__ == '__main__':
    main()
