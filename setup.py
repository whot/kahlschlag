#!/usr/bin/env python

from setuptools import setup

setup(name='kahlschlag',
      version='2',
      description='Selective log monitor and tracker',
      author='Peter Hutterer',
      author_email='peter.hutterer@redhat.com',
      license='GPLv2',
      py_modules=['kahlschlag'],
      entry_points={
          'console_scripts': [ 'kahlschlag = kahlschlag:main' ]
      },
      data_files=[
          ('/usr/lib/systemd/system', ['kahlschlag.service'])
      ],
)
