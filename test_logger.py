#!/usr/bin/python
#
# Note: for $reasons, this is written to run on python3 while the main
# script must be python2 compatible.

import kahlschlag
from kahlschlag import relpath, Git, Timestamps, EventRecorder, MonitoredObject, MonitoredFile, remove_old_logs, make_base_directory
import git
import logging
import os
import struct
import time
import pytest
import yaml

logger = logging.getLogger('test')
logger.setLevel(logging.DEBUG)
fmt = logging.Formatter('%(levelname)-8s| %(message)s')
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(fmt)
logger.addHandler(ch)

klogger = logging.getLogger('kahlschlag')
klogger.setLevel(logging.DEBUG)


def test_relpath():
    assert relpath('foo') == 'foo'
    assert relpath('/foo') == 'foo'
    assert relpath('/foo/bar') == 'foo/bar'
    assert relpath('/foo/bar/') == 'foo/bar/'
    assert relpath('/foo/bar/baz') == 'foo/bar/baz'
    assert relpath('../foo') == '../foo'
    assert relpath('./foo') == './foo'


def test_git_commit(tmp_path):
    repo = Git(tmp_path)
    repo.init()

    # check with real gitpython
    r = git.Repo(tmp_path)
    assert not r.bare
    assert not r.is_dirty()

    for i in range(3):
        with open(tmp_path / 'foo', 'a') as fd:
            fd.write('bar\n')
            fd.flush()
        with open(tmp_path / 'baz', 'a') as fd:
            fd.write('bat\n')
            fd.flush()

        if i == 0:
            assert not r.is_dirty()
            assert r.untracked_files == ['baz', 'foo']
        else:
            assert r.is_dirty()
            assert not r.untracked_files
        repo.add_all()
        assert r.is_dirty()
        assert not r.untracked_files
        repo.commit('commit {}'.format(i))
        assert not r.is_dirty()
        assert not r.untracked_files
        commit = r.head.commit
        assert commit.message.strip() == 'commit {}'.format(i)
        assert commit.author.name == 'kahlschlag'


def test_git_checkout(tmp_path):
    repo = Git(tmp_path)
    repo.init()

    # check with real gitpython
    r = git.Repo(tmp_path)

    with open(tmp_path / 'foo', 'w') as fd:
        fd.write('bar\n')
        fd.flush()
    with open(tmp_path / 'baz', 'w') as fd:
        fd.write('bat\n')
        fd.flush()
    repo.add_all()
    repo.commit('commit 1')

    assert not r.is_dirty()
    assert not r.untracked_files

    with open(tmp_path / 'foo', 'w') as fd:
        fd.write('foobar\n')
        fd.flush()

    assert r.is_dirty()
    assert r.is_dirty(path='foo')
    assert not r.is_dirty(path='baz')

    repo.checkout()
    assert not r.is_dirty()
    assert not r.is_dirty(path='foo')
    assert not r.is_dirty(path='baz')

    assert open(tmp_path / 'foo').read() == 'bar\n'


def test_timestamps(logfs):
    tspath = logfs / 'timestamps'
    ts = Timestamps(12345, logfs)
    assert tspath.exists()
    y = yaml.safe_load(open(tspath))
    assert y['timestamps']
    assert len(y['timestamps']) == 1
    first = y['timestamps'][0]
    assert first['time'] == 12345
    assert first['type'] == 'start'

    for i in range(1, 5):
        ts.update()
        t = ts.current_timestamp
        y = yaml.safe_load(open(tspath))
        assert y['timestamps']
        assert len(y['timestamps']) == i + 1
        ev = y['timestamps'][i]
        assert ev['time'] == t
        assert 'type' not in ev

    ts.stop()
    t = ts.current_timestamp
    y = yaml.safe_load(open(tspath))
    assert y['timestamps']
    assert len(y['timestamps']) == 6
    last = y['timestamps'][5]
    assert last['time'] == t
    assert last['type'] == 'shutdown'


pipe_counter = 0
@pytest.fixture
def pipepath(rootfs):
    global pipe_counter

    pipename = rootfs / 'dev' / 'input' / 'event{}'.format(pipe_counter)
    pipename.parent.mkdir(parents=True, exist_ok=True)
    pipe_counter += 1
    os.mkfifo(pipename)
    yield pipename
    os.unlink(pipename)


def test_evemu_create(logfs, pipepath):
    t1 = Timestamps.timestamp()
    # Need to start evemu first, otherwise our pipe will hang
    evemu = EventRecorder(str(pipepath), basedir=logfs)
    t2 = Timestamps.timestamp()
    evemu.start()

    # we need to open the pipe to the thread to a state where it can be
    # notified to stop again, otherwise we'll hang on shutdown.
    pipe_in = open(pipepath, 'wb')

    time.sleep(1)

    dest = logfs / relpath(str(pipepath))
    assert evemu.dest == str(dest)

    with open(dest) as fd:
        y = yaml.safe_load(fd)
        assert y['device'] == str(pipepath)
        assert t1 <= y['timestamp'] <= t2
        assert 'events' in y

    evemu.stop()
    pipe_in.close()


def test_evemu_events(logfs, pipepath):
    # Need to start evemu first, otherwise our pipe will hang
    evemu = EventRecorder(str(pipepath), basedir=logfs)
    evemu.start()
    logger.info('after start')

    pipe_in = open(pipepath, 'wb')
    eventstruct = struct.Struct('LLHHi')
    events = [list(range(x, x + 5)) for x in range(1, 100, 5)]

    for idx, e in enumerate(events):
        pipe_in.write(eventstruct.pack(*e))
        pipe_in.flush()
        time.sleep(0.02)  # give the thread time to catch up
        with open(evemu.dest) as fd:
            y = yaml.safe_load(fd)
            yevents = y['events']
            assert len(yevents) == idx + 1
            assert yevents[-1]['event'] == e

    evemu.stop()
    pipe_in.close()


def test_evemu_key_obfuscation(logfs, pipepath):
    # Need to start evemu first, otherwise our pipe will hang
    evemu = EventRecorder(str(pipepath), basedir=logfs)
    evemu.start()

    pipe_in = open(pipepath, 'wb')
    eventstruct = struct.Struct('LLHHi')

    # check for key obfuscation
    for key in range(2, 85):
        press = [99, 1, 1, key, 1]
        release = [99, 2, 1, key, 0]
        pipe_in.write(eventstruct.pack(*press))
        pipe_in.write(eventstruct.pack(*release))

    pipe_in.flush()
    time.sleep(0.05)  # give the thread time to catch up

    with open(evemu.dest) as fd:
        y = yaml.safe_load(fd)
        yevents = y['events']
        assert len(yevents) == 83 * 2  # see range(2, 85) up there
        for idx, e in enumerate([ev['event'] for ev in yevents]):
            assert e[0] == 99
            # press/key alternate between 1/2 for usec and 0/1 for value
            assert e[1] == (idx % 2) + 1
            assert e[4] == 1 - (idx % 2)
            assert e[3] == 30  # the obfuscation bit

    evemu.stop()
    pipe_in.close()


def test_evemu_msc_obfuscation(logfs, pipepath):
    # Need to start evemu first, otherwise our pipe will hang
    evemu = EventRecorder(str(pipepath), basedir=logfs)
    evemu.start()

    pipe_in = open(pipepath, 'wb')
    eventstruct = struct.Struct('LLHHi')

    for scancode in range(1, 100):
        msc = [100, 1, 4, 4, scancode]
        pipe_in.write(eventstruct.pack(*msc))
    pipe_in.flush()
    time.sleep(0.05)  # give the thread time to catch up

    with open(evemu.dest) as fd:
        y = yaml.safe_load(fd)
        yevents = y['events']
        assert yevents is not None, y['events']
        assert len(yevents) == 99
        for idx, e in enumerate([ev['event'] for ev in yevents]):
            assert e[0] == 100
            assert e[1] == 1
            assert e[2] == 4
            assert e[3] == 4
            assert e[4] == 30  # the obfuscation bit

    evemu.stop()
    pipe_in.close()


def test_monitored_object():
    # Just testing for AttributeErrors here
    o = MonitoredObject()
    o.start()
    o.stop()
    o.update()
    o.remove()
    assert o.dead is False


def test_monitored_file(rootfs, logfs):
    path = rootfs / 'foo' / 'bar'

    path.parent.mkdir(parents=True, exist_ok=True)
    with open(path, 'w') as fd:
        fd.write('foo')

    strpath = str(path)
    f = MonitoredFile(strpath, basedir=str(logfs))
    assert f.source == strpath
    dest = logfs / relpath(strpath)
    assert f.dest == str(dest)
    assert dest.exists()

    srcdata = open(path).read()
    dstdata = open(dest).read()
    assert srcdata == dstdata

    with open(path, 'w') as fd:
        fd.write('bar')

    f.update()
    assert dest.exists()
    srcdata = open(path).read()
    dstdata = open(dest).read()
    assert srcdata == dstdata

    # removing removes dest only, not source
    f.remove()
    assert path.exists()
    assert not dest.exists()

    f.update()
    assert dest.exists()
    srcdata = open(path).read()
    dstdata = open(dest).read()
    assert srcdata == dstdata

    # if the file goes away, we just keep monitoring it anyway
    path.unlink()
    f.update()
    assert not dest.exists()
    assert not f.dead

    f.remove()  # noop

    # file coming back makes it show up again, with the new value
    with open(path, 'w') as fd:
        fd.write('foo')
    f.update()
    assert dest.exists()
    srcdata = open(path).read()
    dstdata = open(dest).read()
    assert srcdata == dstdata


def test_dir_removal(tmp_path):
    def dirpath(a):
        return os.path.join(tmp_path, a)

    dirs = [dirpath('kahlschlag-{}'.format(d)) for d in range(10)]
    for d in dirs:
        os.makedirs(d)

    os.makedirs(dirpath('canary'))

    # no removal expected
    remove_old_logs(tmp_path, keep=10)
    for d in dirs:
        assert os.path.exists(d)

    keep = 4
    # our sanity checks should complain because these look like our
    # directories but are missing bits
    with pytest.raises(AssertionError):
        remove_old_logs(tmp_path, keep=keep)

    # make them look like real log dirs
    for d in dirs:
        os.makedirs(os.path.join(d, '.git'))
        with open(os.path.join(d, 'timestamps'), 'w') as fd:
            fd.write('content does not matter\n')

    # Now we expect removal to work
    remove_old_logs(tmp_path, keep=keep)
    for d in dirs[-keep:]:
        assert os.path.exists(d)
    for d in dirs[:-keep]:
        assert not os.path.exists(d)

    # make sure we didn't remove our canary
    assert os.path.isdir(dirpath('canary'))


def test_basedir_creation(tmp_path):
    ts = int(time.time())

    expected = time.strftime('%Y-%m-%d-%H:%M:%S', time.gmtime(ts))
    expected = os.path.join(tmp_path, 'kahlschlag-{}'.format(expected))
    assert not os.path.exists(expected)
    assert not os.path.isdir(expected)
    make_base_directory(tmp_path, ts)
    assert os.path.exists(expected)
    assert os.path.isdir(expected)

    prev = expected
    ts += 1
    expected = time.strftime('%Y-%m-%d-%H:%M:%S', time.gmtime(ts))
    expected = os.path.join(tmp_path, 'kahlschlag-{}'.format(expected))
    assert not os.path.exists(expected)
    assert not os.path.isdir(expected)
    make_base_directory(tmp_path, ts)
    # shouldn't touch the existing one
    assert os.path.exists(prev)
    assert os.path.isdir(prev)
    assert os.path.exists(expected)
    assert os.path.isdir(expected)

    prev = expected
    ts += 1
    expected = time.strftime('%Y-%m-%d-%H:%M:%S', time.gmtime(ts))
    expected = os.path.join(tmp_path, 'kahlschlag-{}'.format(expected))
    assert not os.path.exists(expected)
    assert not os.path.isdir(expected)
    make_base_directory(tmp_path, ts, debug=True)
    # shouldn't touch the existing one
    assert os.path.exists(prev)
    assert os.path.isdir(prev)
    # shouldn't have created the new one
    assert not os.path.exists(expected)
    assert not os.path.isdir(expected)
    expected = os.path.join(tmp_path, 'kahlschlag-debug')
    assert os.path.exists(expected)
    assert os.path.isdir(expected)


@pytest.fixture(autouse=True)
def basedir(logfs):
    kahlschlag.BASE_DIRECTORY = logfs


@pytest.fixture(autouse=True)
def log_setup():
    kahlschlag.logger = klogger


@pytest.fixture(scope='function')
def dirtuple(tmp_path_factory):
    '''Create a tuple of directories, one that serves as the root directory,
       one that serves as the log directory (BASE_DIR)'''
    logs = tmp_path_factory.mktemp('logs')
    root = tmp_path_factory.mktemp('root')
    return (root, logs)


@pytest.fixture(scope='function')
def rootfs(tmp_path_factory):
    return tmp_path_factory.mktemp('root')


@pytest.fixture(scope='function')
def logfs(tmp_path_factory):
    return tmp_path_factory.mktemp('logs')
