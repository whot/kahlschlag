kahlschlag is a tool to collect logs from input devices. The motivation
for this tool was to figure out what actually happens when an input device
starts to go wrong, on a system that no developer has access to. kahlschlag
thus logs a set of commands, files, directories (including sysfs) using git.
When the device stops responding, the logs leading up to that time are
then available for debugging.

'Kahlschlag' is the German term for logging a portion of a forest.

Not everything described below is implemented.

# Problem scope

Some devices stop responding at some random time. Logs cannot be gathered
from field devices and the issue is not reproducible in the test setup.
Once the issue occurs, the device will be power-cycled, there is no option
of debugging a live device displaying the issue.

# Goal

This tool collects system state logs from the device on a regular basis with
the goal of providing enough debug data to analyze the device even after a
power off.

Logs are structured after log start date, this tool will write to the same
directory until restarted. The folder strucuture is as follows:

```
2020-04-01-16:00:34/
    .git/
    timestamps
    host/bootid
    host/machine-id
    host/hostname
    host/uptime
    0-lsusb/lsusb.invocation
    0-lsusb/lsusb.stdout
    0-lsusb/lsusb.stderr
    0-lsusb/lsusb.exitcode
    1-dmesg/dmesg.invocation
    1-dmesg/dmesg.stdout
    1-dmesg/dmesg.stderr
    1-dmesg/dmesg.stdexitcode
    2-xinput/xinput.invocation
    2-xinput/xinput.stdout
    2-xinput/xinput.stderr
    2-xinput/xinput.exitcode
    proc/bus/input/devices
    sys/bus/usb/devices/
    sys/bus/hid/devices/
    sys/devices/
    dev/input/event0
    dev/input/event1
    dev/input/event2
```

All files are tracked through git. Each new directory is a separate git
repository and all files are checked into git. Each snapshot will commit the
new state of the system. Information about changes will be extracted during
data analysis with standard git tools.

#  Log files

The `timestamps` file contains the timestamp of each poll start. The first
timestamp is the time at the start of the service.

The `hosts` directory contains various static information about the host.

The number-prefixed directories contains the output of various commands.
These directories are numbered to allow for multiple invocations of the same
command. The `commandname.invocation` file records how the command is run.

## sysfs dump

The `sys` directory contains a dump of the respective `/sys/` directory at
startup time. The sysfs tree is (in addition to polling) monitored via udev,
where a device from those trees is added or removed, the respective sysfs
tree is updated when the event occurs..

The list of devices monitored is limited to the devices applicable for this
bug, i.e. the sysfs trees for each USB device that provides a HID node and
virtual (uinput) devices that provide an event node.

## event recordings 

In the `event` directory, one file represents the event recordings from a
file. The format is YAML in the same style as libinput record, primarily a
series of:

```yaml
  - evdev:
    - [  0,  79965,   3,  53,    2410] # EV_ABS / ABS_MT_POSITION_X      2410 (+23)
    - [  0,  79965,   3,  54,    1644] # EV_ABS / ABS_MT_POSITION_Y      1644 (+88)
    - [  0,  79965,   3,  58,      53] # EV_ABS / ABS_MT_PRESSURE          53 (+1)
    - [  0,  79965,   3,   0,    2410] # EV_ABS / ABS_X                  2410 (+23)
    - [  0,  79965,   3,   1,    1644] # EV_ABS / ABS_Y                  1644 (+88)
    - [  0,  79965,   3,  24,      53] # EV_ABS / ABS_PRESSURE             53 (+1)
    - [  0,  79965,   0,   0,       0] # ------------ SYN_REPORT (0) ---------- +11ms
```

Any event of type `EV_KEY` with an alphanumeric value will be obfuscated to
represent `KEY_A` instead.

## xorg recordings

Kahlschlag needs to run as root to have access to the `/dev/input/event`
nodes and it may be run as a systemd service. It does not have access to the
`$DISPLAY` environment variable but it attempts to find the right one on
startup (or fall back to the default value `:0`). For the root user to have
access to your display, you may need to run `xhost si:localuser:root`.

# Architecture

This is a Python implementation. At the core is a git repository that is
initialized when the service starts, i.e. each timestamped directory is a
separate directory.

The service copies the files into the git repository, adds them and then
periodically copies the current state of the files into the directory again.
After each copy, the timestamp is noted in the `timestamps` file and a git
commit is added with current changes.

On shutdown (`SIGTERM`) a final timestamp is written and a comment is added
to the timestamps file about a successful shutdown. This distinguishes a
system/service shutdown from a poweroff.

The service defaults to polling every 30s. This is primarily so up-to-date
logs exist even where a device turns off internally and no outside event
can be observed. The service reacts to udev events, where a device is added
or removed, the sysfs tree is updated and the other per-device event
recorders are started/stopped.

# Installation

From git, run
```
$ python setup.py install
$ systemctl enable --now kahlschlag.service
```

The second command enables the systemd service to be started after reboot
and starts it as well. kahlschlag will thus start logging immediately.
Please see the `systemctl` documentation for more details.

The `kahlschlag.spec` is provided to simplify building an rpm. Run
```
$ python setup.py sdist
$ rpmbuild -bb kahlschlag.spec --define "_sourcedir $PWD/dist" --define "_rpmdir $PWD"
$ dnf install -y noarch/kahlschlag-*.rpm
$ systemctl enable --now kahlschlag.service
```

## Data storage and usage

Kahlschlag has the potential to collect a lot of data. Two methods are used
to limit how much storage is needed on the target device:

- on each startup, kahlschlag removes old log directories. See the `--keep`
  commandline option.
- kahlschlag automatically terminates after a set period, see the
  `--stop-after` commandline option. A restart will thus cause a cleanup of
  an old log directory.

The latter feature requires systemd to restart kahlschlag when it exits as
enabled in the `.service` file.
