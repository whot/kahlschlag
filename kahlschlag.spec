%if 0%{?rhel} && 0%{?rhel} < 8
%bcond_without legacy_python
%endif

Name:           kahlschlag
Version:        2
Release:        1%{?dist}
Summary:        input device logger

License:        GPLv2+
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  systemd
%if %{with legacy_python}
BuildRequires:  python2-devel
BuildRequires:  python2-setuptools
%else
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
%endif
Requires:       git
Requires:       xorg-x11-server-utils
Requires:       usbutils util-linux

BuildArch:      noarch

%description
%{name} is a tool to log the state of a system including most/all state of
its input devices right up until something goes wrong. The logs created may
help tracking down the actual issue.


%prep
%autosetup


%build
%if %{with legacy_python}
%py2_build
%else
%py3_build
%endif

%install
%if %{with legacy_python}
%py2_install
%else
%py3_install
%endif

%files
%{_bindir}/%{name}
%{_unitdir}/kahlschlag.service
%if %{with legacy_python}
%{python2_sitelib}/%{name}.py
%{python2_sitelib}/%{name}-*.egg-info/
%{python2_sitelib}/%{name}.pyc
%{python2_sitelib}/%{name}.pyo
%else
%{python3_sitelib}/%{name}-*.egg-info/
%{python3_sitelib}/%{name}.py
%{python3_sitelib}/__pycache__/%{name}*.pyc
%endif

%changelog
* Fri May 01 2020 Peter Hutterer <peter.hutterer@redhat.com> 2-1
- Bump to version 2

* Tue Apr 28 2020 Peter Hutterer <peter.hutterer@redhat.com> 1-1
- Initial packaging
